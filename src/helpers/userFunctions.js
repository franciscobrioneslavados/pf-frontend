import axios from "axios";

export const register = (newUser) => {
  return axios({
    method: "POST",
    url: "http://localhost:8080/api/v1/auth/signup",
    data: {
      username: newUser.username,
      password: newUser.password,
    },
  }).then((response) => {
    console.log(response);
    return response;
  }).catch(err => {
    console.error(err);
    return false;
  });
};

export const login = (user) => {
  return axios({
    method: "POST",
    url: "http://localhost:8080/api/v1/auth/signin",
    data: {
      username: user.username,
      password: user.password,
    },
  })
    .then((response) => {
      // console.info(response);
      localStorage.setItem("username", user.username);
      localStorage.setItem("usertoken", response.data["data"]);
      return response
    })
    .catch((err) => {
      console.error(err);
      return false;
    });
};
