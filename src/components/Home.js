import React, { Component } from "react";
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
      page: 1,
    };
  }

  componentDidMount() {
    fetch(`http://localhost:8080/api/v1/characters?page=${this.state.page}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        token: localStorage.usertoken,
        username: localStorage.username,
      },
    })
      .then((res) => res.json())
      .then(
        (result) => {
          console.info(result.characters.results);
          this.setState({
            isLoaded: true,
            items: result.characters.results,
          });
        },
        // Nota: es importante manejar errores aquí y no en
        // un bloque catch() para que no interceptemos errores
        // de errores reales en los componentes.
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      )
      .catch((err) => this.props.history.push("/login"));
  }

  nextPage(){
    
  }

  previewsPage(){

  }

  render() {
    if (localStorage.usertoken) {
      const { error, isLoaded, items } = this.state;
      if (error) {
        return <div>Error: {error.message}</div>;
      } else if (!isLoaded) {
        return <div>Loading...</div>;
      } else {
        return (
          <div className="container">
            <div className="row">
              {items.map((item) => (
                <div key={item.id} className="col-md-4 mt-1">
                  <div className="card">
                    <img src={item.image} className="card-img-top" alt="..." />
                    <div className="card-body">
                      <h5 className="card-title">Name: {item.name}</h5>
                      <p className="card-text">Status: {item.status}</p>
                      <p className="card-text">Species: {item.species}</p>
                      <p className="card-text">Gender {item.gender}</p>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        );
      }
    } else {
      this.props.history.push("/login");
      return false;
    }
  }
}

export default Home;
