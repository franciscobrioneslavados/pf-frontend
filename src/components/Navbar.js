import React, { Component } from "react";
import { Link } from "react-router-dom";

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);
  }


  logOut(e) {
    // e.preventDefault();
    localStorage.removeItem("usertoken");
    localStorage.removeItem("username");
    this.props.history.push("/login");
  }

  render() {
    const loginRegLink = (
      <div className="container">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <h4 className="text-white">Prueba Tecnica</h4>
          </li>
        </ul>
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <Link to="/login" className="nav-link">
              Sig In
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/register" className="nav-link">
              Sign Up
            </Link>
          </li>
        </ul>
      </div>
    );

    const userLink = (
      <div className="container">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <h4 className="text-white">Prueba Tecnica</h4>
          </li>
          <li className="nav-item">
            <Link to="/home" className="nav-link">
              Home
            </Link>
          </li>
        </ul>
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <button
              onClick={this.logOut}
              className="btn btn-outline btn-danger"
            >Logout</button>
          </li>
        </ul>
      </div>
    );

    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark rounded">
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarsExample10"
          aria-controls="navbarsExample10"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div
          className="collapse navbar-collapse justify-content-md-center"
          id="navbarsExample10"
        >
          {localStorage.usertoken ? userLink : loginRegLink}
        </div>
      </nav>
    );
  }
}

export default Navbar;
